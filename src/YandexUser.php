<?php

namespace Drupal\social_auth_yandex;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Yandex User.
 */
class YandexUser implements ResourceOwnerInterface {

  //phpcs:disable
  protected array $response;
  //phpcs:enable.

  /**
   * Create new User.
   */
  public function __construct(array $response) {
    $this->response = $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->response['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function login() {
    return $this->response['login'];
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->response['real_name'] ?? "";
  }

  /**
   * {@inheritdoc}
   */
  public function getDiasplayName(): string {
    return $this->response['display_name'] ?? "";
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstName(): ?string {
    return $this->getResponseValue('first_name');
  }

  /**
   * {@inheritdoc}
   */
  public function getLastName(): ?string {
    return $this->getResponseValue('last_name');
  }

  /**
   * {@inheritdoc}
   */
  public function getLocale(): ?string {
    return $this->getResponseValue('locale');
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail(): ?string {
    return $this->getResponseValue('default_email');
  }

  /**
   * {@inheritdoc}
   */
  public function getEmails(): ?array {
    return $this->getResponseValue('emails');
  }

  /**
   * {@inheritdoc}
   */
  public function getBirthday(): ?string {
    return $this->getResponseValue('birthday');
  }

  /**
   * {@inheritdoc}
   */
  public function getAvatar(): ?string {
    return $this->getResponseValue('default_avatar_id');
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->response;
  }

  /**
   * {@inheritdoc}
   */
  private function getResponseValue($key) {
    return $this->response[$key] ?? NULL;
  }

}

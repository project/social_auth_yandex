<?php

namespace Drupal\social_auth_yandex\Settings;

use Drupal\social_auth\Settings\SettingsInterface;

/**
 * Defines an interface for Social Auth Yandex settings.
 */
interface YandexAuthSettingsInterface extends SettingsInterface {

  /**
   * Gets the restricted domain.
   *
   * @return string|null
   *   The restricted domain.
   */
  public function getRestrictedDomain(): ?string;

}

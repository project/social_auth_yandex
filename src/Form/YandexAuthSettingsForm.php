<?php

namespace Drupal\social_auth_yandex\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Settings form for Social Auth Yandex.
 */
class YandexAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_yandex_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_yandex.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NetworkInterface $network = NULL): array {
    /** @var \Drupal\social_auth\Plugin\Network\NetworkInterface $network */
    $network = $this->networkManager->createInstance('social_auth_yandex');
    $form = parent::buildForm($form, $form_state, $network);

    $config = $this->config('social_auth_yandex.settings');
    $form['info'] = [
      '#weight' => -10,
      '#markup' => $this->t('You need to first create a Yandex App at <a href="@yandex-dev">@yandex-dev</a>',
      ['@yandex-dev' => 'https://oauth.yandex.ru/client/new']
      ),
    ];
    $form['network']['retoken'] = [
      '#type' => 'checkbox',
      '#weight' => -10,
      '#title' => $this->t('Refresh Token'),
      '#description' => $this->t('Refresh Yandex OAuth2 access token with cron.'),
      '#default_value' => $config->get('retoken'),
    ];

    $form['network']['authorized_javascript_origin'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Authorized Javascript Origin'),
      '#description' => $this->t('Copy this value to <em>Authorized Javascript Origins</em> field of your Yandex App settings.'),
      '#default_value' => $GLOBALS['base_url'],
    ];

    $form['network']['advanced']['scopes']['#description'] =
      $this->t('Define any additional scopes to be requested, separated by a comma (e.g.: https://www.yandexapis.com/auth/youtube.upload,https://www.yandexapis.com/auth/youtube.readonly).<br>
        The scopes  \'openid\' \'email\' and \'profile\' are added by default and always requested.<br>You can see the full list of valid scopes and their description <a href="@scopes">here</a>.', [
          '@scopes' => 'https://developers.yandex.com/apis-explorer/#p/',
        ]);

    $form['network']['advanced']['endpoints']['#description'] =
       $this->t('Define the Endpoints to be requested when user authenticates with Yandex for the first time<br>
        Enter each endpoint in different lines in the format <em>endpoint</em>|<em>name_of_endpoint</em>.<br><b>For instance:</b><br>
        /youtube/v3/playlists?maxResults=2&mine=true&part=snippet|playlists_list<br>'
    );

    $form['network']['advanced']['restricted_domain'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Restricted Domain'),
      '#default_value' => $config->get('restricted_domain'),
      '#description' => $this->t('If you want to restrict the users to a specific domain, insert your domain here. For example mycollege.edu. Note that this works only for Yandex Apps hosted accounts.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('social_auth_yandex.settings')
      ->set('client_id', trim($values['client_id']))
      ->set('client_secret', trim($values['client_secret']))
      ->set('scopes', $values['scopes'])
      ->set('endpoints', $values['endpoints'])
      ->set('restricted_domain', $values['restricted_domain'])
      ->set('retoken', $values['retoken'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}

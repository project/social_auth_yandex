<?php

namespace Drupal\social_auth_yandex\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\social_auth\User\SocialAuthUser;
use Drupal\social_auth\User\SocialAuthUserInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains all the logic for Yandex OAuth2 authentication.
 */
class YandexAuthManager extends OAuth2Manager {

  // phpcs:disable
  protected string $tok;
  protected mixed $client = NULL;
  protected ?Request $request;
  protected string $email;
  protected string $userId;
  protected string $state;
  // phpcs:enable

  /**
   * Constructor.
   */
  public function __construct(ConfigFactory $configFactory,
                              LoggerChannelFactoryInterface $logger_factory,
                              RequestStack $request_stack) {
    parent::__construct($configFactory->get('social_auth_yandex.settings'),
                        $logger_factory,
                        $request_stack->getCurrentRequest());
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate() {
    $code = $this->request->query->get('code');
    try {
      $tok = $this->client->getAccessToken('authorization_code', [
        'code' => $code,
      ]);
      $this->setAccessToken($tok);
    }
    catch (IdentityProviderException $e) {
      $err = $e->getMessage();
      $this->loggerFactory->get('social_auth_yandex')
        ->error("There was an error during authentication. Exception: $err");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo(): SocialAuthUserInterface {
    if (!$this->user) {
      /** @var \Drupal\social_auth_yandex\YandexUser $owner */
      $owner = $this->client->getResourceOwner($this->getAccessToken());
      $this->user = new SocialAuthUser(
        $owner->getName(),
        $owner->getId(),
        $this->getAccessToken(),
        $owner->getEmail(),
        $owner->getAvatar(),
        $this->getExtraDetails()
      );
      $this->user->setFirstName($owner->getFirstName());
      $this->user->setLastName($owner->getLastName());
      if ($ava = $owner->getAvatar()) {
        $this->user->setPictureUrl("https://avatars.yandex.net/get-yapic/{$ava}/islands-200");
        // $this->user['picture_url'] = "https://avatars.yandex.net/get-yapic/{$ava}/islands-200";
      };
      if (FALSE) {
        // @todo Old Code:
        $data = $owner->toArray();
        $this->user = [
          'id' => $owner->getId(),
          'login' => $data['login'],
          'client_id' => $data['client_id'],
          'email' => $data['default_email'],
          'real_name' => $data['real_name'],
          'first_name' => $data['first_name'],
          'last_name' => $data['last_name'],
        ];
      }
    }
    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): string {
    $scopes = [
      // 'openid',
      // 'profile',
      // 'email',
    ];

    $extra_scopes = $this->getScopes();
    if ($extra_scopes) {
      $scopes = array_merge($scopes, explode(',', $extra_scopes));
    }

    // Returns the URL where user will be redirected.
    $url = $this->client->getAuthorizationUrl([
      // 'scope' => $scopes,
    ]);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->client->getState();
  }

  /**
   * Get redirect url.
   *
   * @return string
   *   The redirect url.
   */
  public function getRedirectUrl() {
    return Url::fromRoute('social_auth_yandex.callback')->setAbsolute()->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraDetails(string $method = 'GET', ?string $domain = NULL): ?array {
    $tok = $this->getAccessToken();
    $user = $this->client->getResourceOwner($tok)->toArray();
    return [
      'token' => $tok,
      'user' => $user,
    ];
  }

  /**
   * Skip.
   */
  public function requestEndPoint(string $method, string $path, ?string $domain = NULL, array $options = []): mixed {
    // Do nothing.
  }

}

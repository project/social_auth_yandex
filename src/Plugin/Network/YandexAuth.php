<?php

namespace Drupal\social_auth_yandex\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines a Network Plugin for Social Auth Yandex.
 *
 * @package Drupal\social_auth_yandex\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_yandex",
 *   short_name = "yandex",
 *   social_network = "Yandex",
 *   img_path = "img/yandex_logo.svg",
 *   type = "social_auth",
 *   class_name = "\Drupal\social_auth_yandex\YandexAuthProvider",
 *   auth_manager = "\Drupal\social_auth_yandex\Service\YandexAuthManager",
 *   routes = {
 *     "redirect": "social_auth.network.redirect",
 *     "callback": "social_auth.network.callback",
 *     "settings_form": "social_auth.network.settings_form",
 *   },
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_yandex\Settings\YandexAuthSettings",
 *       "config_id": "social_auth_yandex.settings"
 *     }
 *   }
 * )
 */
class YandexAuth extends NetworkBase implements NetworkInterface {

  /**
   * {@inheritdoc}
   */
  protected function getExtraSdkSettings(): array {
    $hosted_domain = $this->settings->getRestrictedDomain();
    return [
      'accessType' => 'offline',
      'verify' => FALSE,
      'hostedDomain' => empty($hosted_domain) ? NULL : $hosted_domain,
    ];
  }

}

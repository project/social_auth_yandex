<?php

namespace Drupal\social_auth_yandex;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Defines methods to get Social Auth Yandex settings.
 */
class YandexAuthProvider extends AbstractProvider {

  use BearerAuthorizationTrait;

  /**
   * {@inheritdoc}
   */
  public function getBaseAuthorizationUrl(): string {
    return 'https://oauth.yandex.ru/authorize';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAccessTokenUrl(array $params): string {
    return "https://oauth.yandex.ru/token";
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceOwnerDetailsUrl(AccessToken $token): string {
    return 'https://login.yandex.ru/info';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultScopes(): array {
    // "openid" MUST be the first scope in the list.
    return [
      // 'openid',
      // 'email',
      // 'profile',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function checkResponse(ResponseInterface $response, $data): void {
    // @codeCoverageIgnoreStart
    if (empty($data['error'])) {
      return;
    }
    // @codeCoverageIgnoreEnd
    $code = 0;
    $error = $data['error'];

    if (is_array($error)) {
      $code = $error['code'];
      $error = $error['message'];
    }

    throw new IdentityProviderException($error, $code, $data);
  }

  /**
   * {@inheritdoc}
   */
  protected function createResourceOwner(array $response, AccessToken $token): YandexUser {
    $user = new YandexUser($response);
    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    if (!$this->state) {
      $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $length = 32;
      $this->state = substr(str_shuffle(str_repeat($chars, ceil($length / strlen($chars)))), 1, $length);
    }
    return $this->state;
  }

}
